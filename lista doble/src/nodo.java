
public class nodo {
	private nodo anterior;                                  //apunta al nodo anterior
	private nodo posterior;									//apunta al nodo siguiente
	private int dato;										//el valor que tiene el nodo
	
	//Crea el nodo a partir de otro (clona el nodo)
	
	public nodo(nodo nodo){
		anterior = nodo.anterior;
		posterior = nodo.posterior;
		dato = nodo.dato;
	}
	
	//Crea el nodo a partir de un valor (es el primer nodo creado)
	
	public nodo(int dato){
		anterior = null;
		posterior = null;
		this.dato = dato;
	}
	
	//Crea un nodo dandole un dato, un nodo antrior y uno siguiente
	
	public nodo(int dato, nodo anterior, nodo posterior){
		this.anterior = anterior;
		this.posterior = posterior;
		this.dato = dato;
	}
	
	//Busca el primer nodo
	
	public nodo buscarPrimero(){
		nodo nodoAuxiliar = new nodo(this);
		while(nodoAuxiliar.anterior != null){
			nodoAuxiliar = new nodo(nodoAuxiliar.anterior);
		}
		return nodoAuxiliar;
	}
	
	//Busca el ultimo nodo
	
	public nodo buscarUltimo(){
		nodo nodoAuxiliar = new nodo(this);
		while(nodoAuxiliar.posterior != null){
			nodoAuxiliar = nodoAuxiliar.posterior;
		}
		return nodoAuxiliar;
	}
	
	//inserta un nodo por la derecha
	
	public void insertarDerecha(int dato){
		nodo nodoAuxiliar = this.posterior;
		if(nodoAuxiliar == null){                         //No tiene posteriores
			nodoAuxiliar = new nodo(dato, this, null);
			this.posterior = nodoAuxiliar;
		}else{
			nodo nodoAuxiliar2 = nodoAuxiliar.posterior;
			if(nodoAuxiliar2 == null){                    //Tiene un posterior a el
				nodoAuxiliar2 = new nodo(nodoAuxiliar.dato, nodoAuxiliar, null);
				nodoAuxiliar.dato = dato;
				nodoAuxiliar.posterior = nodoAuxiliar2;
			}else{                                        //Si tiene mas de dos nodos posteriores a el
				while(nodoAuxiliar2 != null){             //Va hasta el ultimo nodo y su posterior
					nodoAuxiliar = nodoAuxiliar2;
					nodoAuxiliar2 = nodoAuxiliar2.posterior;
				}
				nodoAuxiliar2 = new nodo(nodoAuxiliar.dato, nodoAuxiliar, null); //Mueve el ultimo a su posterior
				nodoAuxiliar.posterior = nodoAuxiliar2;
				while(nodoAuxiliar.anterior != this){     //Mueve los nodos entre el nuevo ultimo y el nodo principal hacia la "derecha"
					nodoAuxiliar2.dato = nodoAuxiliar.dato;
					nodoAuxiliar2 = nodoAuxiliar;
					nodoAuxiliar = nodoAuxiliar.anterior;
				}
				nodoAuxiliar2.dato = nodoAuxiliar.dato;   //A�ade el nuevo nodo en el "hueco que se deja"
				nodoAuxiliar.dato = dato;
			}
		}
	}
	
	//inserta un nodo por la izquierda
	
	public void insertarIzquierda(int dato){
		nodo nodoAuxiliar = this.anterior;
		if(nodoAuxiliar == null){                      //No tiene nodo anterior
			nodoAuxiliar = new nodo(dato, null, this);
			this.anterior = nodoAuxiliar;
		}else{                                         //Tiene solo un nodo anteriores a el
			nodo nodoAuxiliar2 = nodoAuxiliar.anterior;
			if(nodoAuxiliar2 == null){
				nodoAuxiliar2 = new nodo(nodoAuxiliar.dato);
				nodoAuxiliar2.anterior = null;
				nodoAuxiliar2.posterior = nodoAuxiliar;
				nodoAuxiliar.dato = dato;
				nodoAuxiliar.anterior = nodoAuxiliar2;
			}else{                                    //Si tiene mas de dos nodos anteriores a el
				while(nodoAuxiliar2 != null){		  //Va hasta el primer nodo y su anterior
					nodoAuxiliar = nodoAuxiliar2;
					nodoAuxiliar2 = nodoAuxiliar2.anterior;
				}
				nodoAuxiliar2 = new nodo(nodoAuxiliar.dato, null, nodoAuxiliar);//Mueve el primero a su anterior
				nodoAuxiliar.anterior = nodoAuxiliar2;
				while(nodoAuxiliar.posterior != this){//Mueve los nodos entre el nuevo primero y el nodo principal hacia la "izquierda"
					nodoAuxiliar2.dato = nodoAuxiliar.dato;
					nodoAuxiliar2 = nodoAuxiliar;
					nodoAuxiliar = nodoAuxiliar.posterior;
				}
				nodoAuxiliar2.dato = nodoAuxiliar.dato;//A�ade el nuevo nodo en el "hueco que se deja"
				nodoAuxiliar.dato = dato;
			}
		}
	}
	
	//Obtiene el dato
	
	public int getDato() {
		return dato;
	}
	
	//Obtiene el posterior
	
	public nodo getPosterior(){
		return posterior;
	}
	
	//Obtiene el anterior
	
	public nodo getAnterior(){
		return anterior;
	}
	
	//Muestra el dato del nodo junto con el anterior y el siguiente (si tiene alguno de los dos)
	
	public String toString(){
		nodo nodoAuxiliar = this;
		if(posterior == null && anterior == null){
			return "" + nodoAuxiliar.getDato();
		}
		if(posterior == null){
			return nodoAuxiliar.anterior.getDato() + "| " + nodoAuxiliar.getDato();
		}
		if(anterior == null){
			return "  | " + nodoAuxiliar.getDato() + "| " + nodoAuxiliar.posterior.getDato();
		}
		return  nodoAuxiliar.anterior.getDato() + "| " + nodoAuxiliar.getDato() + "| " + nodoAuxiliar.posterior.getDato();


	}
}
