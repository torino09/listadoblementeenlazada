
//La lista debe crearse cuando los nodos est�n creados, no antes, porque sino no obtiene el verdadero primer y ultimo nodo correctos

public class lista {
	private nodo primerNodo;
	private nodo ultimoNodo;
	
	public lista(nodo nodo){
		primerNodo = nodo.buscarPrimero();//Asigna el primer nodo y el ultimo a sus nodos correspondientes en la lista
		ultimoNodo = nodo.buscarUltimo();
	}
	
	//obtiene el primer nodo
	
	public nodo getPrimero(){
		return primerNodo;
	}
	
	//Obtiene el ultimo nodo
	
	public nodo getUltimo(){
		return ultimoNodo;
	}
	
	//Se muestra el primer y ultimo nodo
	
	public String toString(){
		return "Primer nodo: " + getPrimero().getDato() + "\nUltimo nodo: " + getUltimo().getDato();
	}
}
