import java.util.Random;

public class test {
	
	private static int N = 50; //Numero de nodos a dar aleatoriamente
	
	public static void main(String[] args) {
		nodo nodo = new nodo((new Random()).nextInt());
		for(int a = 0; a < N; a++){//En esas 50 iteraciones se inserta un nodo por la derecha y otro por la izquierda alternandose
			if(a%2 == 0){
				nodo.insertarIzquierda((new Random()).nextInt());
			}
			if(a%2 == 1){
				nodo.insertarDerecha((new Random()).nextInt());
			}
		}
		lista lista = new lista(nodo); //Se crea la "lista" para saber el primer y ultimo nodo
		System.out.println(lista.toString() + "\nAnterior "+"Dato " + "Siguiente"); // Se muestra el primer y ultimo nodo junto con la cabecera "Anterior Dato Siguiente"
		nodo n = lista.getPrimero();//Se crea un nodo asignando el primer nodo
		while(n != null){
			System.out.println(n.toString());//Se muestra los nodos desde el primero hasta el ultimo
			n = n.getPosterior();
		}
	}

}
